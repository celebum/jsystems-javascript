```js

// Product:
// - id 
// - name
// - description
// - price
// - promotion

// Hidden Class
var product = {
    id: '',
    name: "",
    description: "",
    price: 0,
    promotion: false,
}

// Dictionary / HashMap
product = {}
product.id = 1
// product['name'] = 'Woda'
product.name = 'Woda'
product.description = 'Woda niegazowana'
product.price = 3.5
product.promotion = true

product.name

// name = 'jablko'; // window.name = 'jablko'

var id = 2;
var name = 'jablko';
var description = 'desc test';
var price = 1.4;
var promotion = true;

// product2 = { id:id, name:name, description:description, price:price, promotion:promotion };
product2 = { id, name, description, price, promotion };

console.log(product)


// console.log(id,name,desc...);
// console.log(product);
```


const tax = 23;

const products = [
    { id: '123', name: 'Placki', category: 'placki', price: 9750, discount: 0.33, promoted: true },
    { id: '234', name: 'Naleśniki', category: 'nalesniki', price: 5650, discount: 0, promoted: false },
    { id: '345', name: 'Pączki', category: 'ciastka', price: 3050, discount: 0.2, promoted: true },
]

// debugger

// Filters:
let isPromoted = true  /* false | undefined */
let limit = 2;
// var direction = -1 /* -1 */

var i = 0, count = 0;
if(products.length){
    productsList.children[0].remove()
}
while (i < products.length) {
    const product = products[i++]

    if (isPromoted !== undefined && !(product.promoted === isPromoted)) continue
    if (count++ == limit) break;

    // Switch
    let slogan = ''
    switch (product.category) {
        case 'nalesniki':
            slogan += 'Najlepsze nalesniki i ';
        case 'placki':
            slogan += 'Pyszne placki';
            break;

        case 'ciastka': slogan = 'Pyszne ciastka'; break;
    }

    // Loop
    const extra = (product.promoted ? "[PROMOCJA " + product.discount * 100 + "%]" : "")
    const netto = product.price - (product.price * product.discount)
    const brutto = netto * (1 + (tax / 100))
    const price = (brutto / 100).toFixed(2) + 'zł'

    // console.log(product.name + ' - ' + price + 'zł ' + extra);
    console.log(`${count} ${product.name} - ${price} ${extra} ${slogan}`);

    div = document.createElement('div')
    div.className = 'list-group-item';
    div.innerText = `${count} ${product.name} - ${price} ${extra} ${slogan}`
    productsList.append(div)
}
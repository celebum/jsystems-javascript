var tax = 23;

var products = [
    { id: '123', name: 'Placki', category: "nalesniki", price: 97.50, discount: 0.1, promoted: true },
    { id: '234', name: 'Nalesniki', category: "nalesniki", price: 56.50, discount: 0, promoted: false },
    { id: '345', name: 'Paczki', category: "ciastka", price: 30.50, discount: 0.20, promoted: true }
]

var isPromoted = true;
var limit = 2;

for (var key in products) {

    var product = products[key];
    var netto = product.price + (product.price * product.discount);
    var brutto = netto * (1 + (tax / 100));
    var extra = product.promoted ? " PROMOCJA zl {" + product.discount * 100 + " % }" : "";
    var slogan = "";

    switch (product.category) {
        case "nalesniki":
            slogan += " Najlepsze nalesniki";
            break;
        case "ciastka":
            slogan += " Pyszne ciastka";
            break;
    }

    if (limit > 0) {
        //debugger // aktuwje breakpoint
        if (product.promoted = isPromoted) {
            console.log(product.name + " - " + brutto.toFixed(2) + extra + slogan);
            limit--;
        }
    } else {
        break;
    }
}

//console.log('Placki - 97.50 zl {PROMOCJA - 30%}');